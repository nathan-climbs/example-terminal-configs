# example configuration for Alacritty, Kitty and Wezterm

All three of these terminals have good documentation on their respective configuration. 

All of the example configurations use the IntoneMono Nerd Font font. You can download this font from https://www.nerdfonts.com/
or change the config file to use a different font.

## Alacritty

https://alacritty.org/

Alacritty switched from yaml to toml for their configuration with version 13. If you have v13+ use toml.

Config locations:
- Linux and MacOS `~/.config/alacritty/alacritty.toml`
- Windows `%APPDATA%\alacritty\alacritty.toml`

## Kitty

https://sw.kovidgoyal.net/kitty/

Config locations:
- Linux and MacOS `~/.config/kitty/kitty.conf`

## Wezterm

https://wezfurlong.org/wezterm/index.html

Config locations:
- Linux and MacOS `~/.config/wezterm/wezterm.lua`
- Windows `$HOME/.wezterm.lua`
