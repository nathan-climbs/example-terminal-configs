-- wezterm example config jan 2024
local wezterm = require 'wezterm'
return {
	color_scheme = 'Catppuccin Mocha',
	-- color_scheme = 'rose-pine',
	colors = {
  	cursor_bg = '#ea999c',
  	cursor_fg = 'black',
	},
	enable_tab_bar = false,
	font = wezterm.font {
		family = 'IntoneMono Nerd Font'
		-- harfbuzz_features = { 'ss01', 'ss02', 'ss03', 'ss04', 'ss05', 'ss06', 'ss07', 'ss08', 'calt', 'dlig' },
		-- weight = 'DemiBold'
	},
	font_size = 14.0,
	default_cursor_style = 'SteadyUnderline',
	underline_thickness = 3.0,
	window_background_opacity = 1.0,
	window_padding = {
  	left = 8,
  	right = 8,
  	top = 5,
  	bottom = 5,
	},
	window_decorations = 'NONE',
	keys = {
		{
			key = 'f',
			mods = 'SUPER',
			action = wezterm.action.ToggleFullScreen,
		},
		{
			key = 'q',
			mods = 'SUPER',
			action = wezterm.action.QuitApplication,
		},
	},
}
